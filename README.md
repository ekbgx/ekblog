### `ekblog`


### Getting started

Clone this project:

```
git clone git@gitlab.com:ekbgx/ekblog.git
```

Install [hugo](https://gohugo.io/):

```
apt install hugo    # install hugo via package manager
```

Build blog:

```
hugo server -D      # local test
hugo -D             # generate static resources
```

### License

**The text in my blog passages** are Licensed under `CC BY-NC-ND 4.0`.

Blog theme [blank](https://github.com/Vimux/blank) is licensed under MIT.

Other resources which are not licensed belongs to their original authors.

Feel free to report license violations in this blog to me, I will delete related resources as soon as I can. 


