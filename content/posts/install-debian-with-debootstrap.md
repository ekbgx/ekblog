---
title: "使用 debootstrap 安装 Debian 系统"
date: 2022-10-05T11:31:12+08:00
draft: false
---

### 写在前面

在最开始我想说明的是：为何选择 Debian？

我的体会是，Debian 是许许多多优秀的发行版的“母亲”，当我在入大学的暑假了解到了 Linux 操作系统后，我首先知道的是 Ubuntu，稍加搜索后就知道了其上游——Debian。同时了解到 Debian “哺育”了众多的发行版。我希望了解这一伟大的操作系统，从此走上了使用 Debian GNU/Linux 的道路。

Debian 的优点：

- stable 版本很稳定。Debian stable 经过细致的测试后才会发布，发行周期较长。虽然会有老旧的问题，但对于服务器等可能不需要频繁更新以使用新特性的场景来说，stable 是一个好的选择。
- sid （不稳定版本）的软件包足够新、质量足够好。Debian 的 sid 处在 Debian 开发的前沿，与 stable 不同，sid 会频繁地收到软件更新，能使用更新的软件。同时，得益于规范严格的 [Debian Policy](https://www.debian.org/doc/debian-policy/)，即便 sid 软件包通常只经过了简单的测试，但质量依然有保障。
- 多架构。Debian 宣称自己为“通用操作系统”，令人感到高兴的是，Debian 为此做出了努力并取得了成效。到目前为止 Debian 是支持架构最多的发行版之一 ([支持列表](https://wiki.debian.org/SupportedArchitectures))。

这是我大学几年中业余探索出的一些微小的成果，十分愿意与大家一起分享，遵照自由开源的精神将知识传递出去（许可证可以在 [GitHub 仓库](https://github.com/ketlrznt/ekblog) 找到）。

### 0. 工具准备

推荐使用 [Ubuntu liveCD](https://ubuntu.com/) 进行安装。
需要额外下载的包：`debootstrap`，用于安装 Debian RootFS。`arch-install-scripts`，包含 `genfstab` 指令，可以方便地生成 `fstab` 文件。
接下来的所有指令以 su 权限执行。进入 Ubuntu 环境后可以打开终端，输入 `sudo su` 进入 su。

### 1. 磁盘的准备

安装系统首先需要准备好磁盘。建议对一块完整的磁盘执行操作：
**以 nvme 硬盘为例子，硬盘标记为 /dev/nvme0n1**。
分三个区域：

```bash
/boot/efi     # 启动分区
/             # 根目录
/home         # 家目录
```

```bash
parted /dev/nvme0n1                   # 使用 parted 操作 /dev/nvme0n1

(parted) matable                      # 新建分区表
New disk label type? gpt              # 选择 gpt 类型分区表，如果磁盘内原本有数据会警告，输入 yes
quit                                  # 建表完毕，退出
```

接着使用 `cfdisk` 对磁盘进行分区。其中，启动分区 （它将是 `/boot/efi`）需要为第一个分区，并选择 `EFI System` 类型，建议大小 1G；根目录分区建议大小 100G，其余全部分配给家目录分区。根目录和家目录选择 `Linux filesystem` 类型。

```
cfdisk /dev/nvme0n1                   # 对磁盘分区
```

分区结束后，依照在磁盘内的顺序，不同的分区也会有对应的编号，在 nvme 磁盘下是在原始磁盘标识后加上 `p<num>`，`<num>` 标识第 `<num>` 个分区。启动分区是磁盘内的第一个分区，它的标识就是 `/dev/nvme0n1p1`。

接着对不同分区进行格式化：

```bash
mkfs.vfat /dev/nvme0n1p1             # 格式化启动分区为 vfat
mkfs.btrfs  /dev/nvme0n1p2
mkfs.btrfs  /dev/nvme0n1p3           # 格式化另外两个分区为 btrfs
```

### 2. 安装基础系统

随后，建立目录，挂载分区。注意要先挂载 **根目录**。

```bash
mkdir /mnt/debian                      # 建立根目录文件夹
mount /dev/nvme0n1p2 /                 # 挂载根目录
mkdir -pv /mnt/debian/boot/efi         # 建立启动分区目录
mkdir -pv /mnt/home                    # 建立家目录
mount /dev/nvme0n1p1 /boot/efi         # 挂载启动分区
mount /dev/nvme0n1p3 /home             # 挂载家目录
```

接下来使用 `debootstrap` 安装基础系统：

```bash
debootstrap --include btrfs-progs,locales,linux-image-amd64,grub-efi --arch amd64 sid /mnt/debian https://mirrors.ustc.edu.cn/debian
```

其中 `btrfs-progs` 用于 btrfs 文件系统下内核相关文件的生成。`grub-efi` 是之后需要的 `grub` 系统启动工具。`sid` 为 Debian 不稳定发行版的代号，`/mnt/debian` 标识系统安装位置（根目录），最后的网址为安装时选用的镜像地址，会写入新系统的 `/etc/apt/sources.list` 文件中。

系统安装完毕后，先生成 fstab 文件，fstab 文件内记录了文件系统分区相关信息：

```bash
genfstab -U /mnt/debian >> /mnt/debian/etc/fstab
```

随后，需要执行额外的操作，以让随后的新系统在 chroot 进入后正常工作。

```bash
mount --rbind /sys /mnt/debian/sys && mount --make-rslave /mnt/debian/sys
mount --rbind /dev /mnt/debian/dev && mount --make-rslave /mnt/debian/dev
mount --rbind /proc /mnt/debian/proc && mount --make-rslave /mnt/debian/proc

# chroot 进入子目录下的系统
chroot /mnt/debian
```

或者使用 `arch-chroot`（由 `arch-install-scripts` 提供）完成 `chroot` 系列操作：

```bash
arch-chroot /mnt/debian
```

执行完毕后，处于新系统的 `chroot` 环境下。

### 3. 安装基础软件和设置

首先修改 `/etc/apt/sources.list`，此前它已经有一条记录，包含 Debian 的 main 仓库。此时需要补充另外两个 contrib 和 non-free 仓库以满足非自由固件需要（如网卡、蓝牙驱动等）。

```bash
nano /etc/apt/sources.list

deb https://mirrors.ustc.edu.cn/debian sid main contrib non-free # 在编辑器内 main 后新增 contrib 和 non-free 字段，保存退出

apt update                 # 刷新 apt 仓库
```

随后安装必要软件：

```bash
apt install sudo                # sudo
apt install zsh                 # zsh 是一个方便易用的 shell
apt install firmware-linux      # 固件
apt install firmware-iwlwifi    # Intel 网卡驱动，如有需要选用
apt install firmware-realtek    # realtek 驱动，含蓝牙
apt install neovim              # 方便的编辑工具
```

修改主机名：

```bash
nano /etc/hostname        # 填入自己喜欢的主机名（建议全小写），如 debian
nano /etc/hosts
```

将 `/etc/hosts` 修改为：

```bash
127.0.0.1    localhost
::1          localhost
127.0.1.1    <hostname>
```

hostname 部分填入之前的主机名。

设置 root 密码：

```bash
passwd
```

添加用户，例如用户名为 testuser：

```bash
useradd -m -s /bin/bash testuser    # testuser 换为自己喜欢的用户名（小写）
```

将用户加入 sudoers：

```bash
usermod -aG sudo testuser
```

为 testuser 设置密码：

```bash
passwd testuser
```

配置时区和 locale：

```bash
dpkg-reconfigure tzdata          # 配置时区，中国大陆选择 Asia/Shanghai
dpkg-reconfigure locales         # 配置 locales，选择 en_US.UTF-8  和 zh_CN.UTF-8
```

安装 grub：

```bash
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=Debian # 安装 grub

update-grub    # 更新 grub
```

`update-grub` 会从 `/boot/efi` 中找到开机所需要的内核与 initramfs 镜像，生成 grub 启动必须的配置。

### 4. 安装桌面环境

推荐使用 `GNOME` 桌面环境。Wiki 上安装的 `gnome-core` 我认为并不 “精简”。此应当为我所能找到的最简单配置。

```bash
apt install gnome-shell            # GNOME shell 环境
apt install gnome-terminal         # GNOME 提供的终端模拟器
apt install gdm3                   # GNOME 桌面管理器，提供了 GNOME shell 登陆相关功能
apt install nautilus               # GNOME 文件管理器
```

### 5. 结束

至此完成所有基础安装流程。退出 chroot 环境，解除挂载：

```bash
exit
umount -R /mnt/debian
```

### 6. 其他软件

<p align="center">
<img src="https://s2.loli.net/2022/09/03/T2oYBdn4SaPieLf.png" alt="Zelda by ALzi米" width="1000px">
</p>

[壁纸图源 ALzi米](https://twitter.com/AlzzziMi/status/1431608364681756673)

进入桌面后，打开终端，安装其他可能需要的软件：

```bash
apt install firefox             # 火狐浏览器 
sudo apt install mpv            # mpv 视频播放器
sudo apt install eog            # GNOME 之眼图像查看程序
sudo apt install evince         # evince 文档查看器
sudo apt install rhythmbox      # rhythmbox 音乐播放器
sudo apt install geany          # 依赖极少的图形化文本编辑器
```

### 7. 推荐

#### 7.1 字体

一般而言，下载 `fonts-noto` 字体包即可应对大多数情况。

编程用等宽字体个人推荐 **JetBrains Mono Nerd Fonts**。~~（毕竟是 JB 自家的东西得支持一下）~~。效果不错。

Nerd Fonts 下载地址：https://www.nerdfonts.com/font-downloads

#### 7.2 终端相关

zsh 终端可以用 [oh-my-zsh](https://ohmyz.sh/) 以提供更多的功能（我比较懒，oh-my-zsh 做好了大多数我需要的功能）。主题可以尝试一下我做的 [ketlrznt.zsh-theme](https://github.com/ketlrznt/ketlrznt.zsh-theme)。

GNOME terminal 主题使用 [Gogh](https://github.com/Gogh-Co/Gogh) 下载 profile，我目前选用的是 **Tokyo Night Storm**。

### 结束语

许多人希望参与到自由开源中来，或者自由地分享自己所学（不仅限于软件开发）。除了自由开源软件之外，我认为许许多多的内容都可以参考软件自由开源的精神进行分享。我在 Debian 相关内容的学习、安卓编程的学习中，得到了许多帮助和参考，这些帮助无一例外的都是**自由**的，人们公开地分享自己的知识并授予在一定条件下再利用的权利，是我认为的理想的互联网学习状态。
对我来说，我的主力工具就是电脑，那么，使用一个自由开源的操作系统工作、学习，就是我的整个自由之旅的重要的第一步。
