---
title: "about"
date: 2022-10-13T12:49:55+08:00
draft: false
---

## Current State

Debian GNU/Linux | Android | Kotlin | Free and Open Source Softwares

是在备战二战考研的无聊家里蹲计科大学生。喜欢 Linux / Kotlin / Android。日常倒腾 Debian GNU/Linux。

读了这么多年书，大学还算可以，顺利地毕业了，坚持到现在，有什么理由呢？一切的理大概是出于 [纯爱](https://zh.wikipedia.org/wiki/%E7%BA%AF%E7%88%B1)  理念。不断前进，有能力给自己喜欢人最好的生活，培养自己的兴趣爱好，非常重要，是我理想中的生活状态（然而目前依然是单身宅男且程序写得稀烂）。

日常喜欢 NACG，如 Steins;Gate / Angel Beats! / Key 的其他游戏之类。

目前把精力集中在学习上。再努力争取一次。

感觉也需要借此塑造学习态度。考研本身能学到的字面知识终究有限。

## Links:

GitLab: [www.gitlab.com/ekbgx](https://www.gitlab.com/ekbgx)

Mail: [tansuanyinliao8888@gmail.com](mailto:tansuanyinliao8888@gmail.com)

## Source Code

You can get the source code of my blog on GitLab:
[ekblog](https://gitlab.com/ekbgx/ekblog)

